package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"gitlab.com/thesilk/transfer-me/app/controller"
	"gitlab.com/thesilk/transfer-me/app/models"
	"gitlab.com/thesilk/transfer-me/app/repository"
	"gitlab.com/thesilk/transfer-me/app/service"
)

var (
	version string
)

func initCLI() {
	helpFlag := flag.Bool("help", false, "help")
	versionFlag := flag.Bool("version", false, "version")
	flag.Parse()

	if *helpFlag {
		flag.Usage()
		os.Exit(0)
	}

	if *versionFlag {
		fmt.Println(version)
		os.Exit(0)
	}
}

func initConfig() {
	viper.SetConfigName("config")             // name of config file (without extension)
	viper.SetConfigType("yaml")               // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("/etc/transfer-me/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.transfer-me") // call multiple times to add many search paths
	viper.AddConfigPath(".")                  // optionally look for config in the working directory

	if err := viper.ReadInConfig(); err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
}

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	initCLI()
	initConfig()

	if viper.GetBool("production") {
		gin.SetMode(gin.ReleaseMode)
	}

	validToken := viper.GetString("valid_token")

	s3 := models.S3Storage{}
	viper.UnmarshalKey("s3", &s3)

	repository, err := repository.NewRepository(s3)
	if err != nil {
		log.Fatalln(err)
	}

	encryptionService := service.NewEncryptionService(viper.GetString("secret"))
	s3Service := service.NewS3FileService(repository, encryptionService)
	zipSteamerService := service.NewZipStreamerService(repository, encryptionService)

	uploadHandler := controller.PutFileUpload(s3Service, validToken)
	downloadHandler := controller.GetDownloadFile(s3Service)
	bundleHandler := controller.PostDownloadBundle(s3Service, zipSteamerService)
	bundleStatsHandler := controller.GetDownloadStatsForBundle(s3Service, validToken)
	authHandler := controller.PostAuth(validToken)

	router := gin.New()
	// if route panics it will send a 500
	router.Use(gin.Recovery())
	router.MaxMultipartMemory = 2 * 1024 * 1024 * 1024 // 2 GiB

	router.POST("/api/v0/auth", authHandler)
	router.PUT("/api/v0/upload", uploadHandler)
	router.POST("/api/v0/download/:bundle/:id", downloadHandler)
	router.POST("/api/v0/bundle/:bundle", bundleHandler)
	router.GET("/api/v0/bundle/:bundle", bundleStatsHandler)
	router.NoRoute(gin.WrapH(http.FileServer(gin.Dir("public", false))))
	// router.Static("/", "./public")

	adapter := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", viper.GetString("server_host"), viper.GetUint16("server_port")),
		Handler: router,
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	go func() {
		adapter.ListenAndServe()
	}()
	<-stop

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	adapter.Shutdown(ctx)

	select {
	case <-ctx.Done():
		log.Println("timeout of 3 seconds.")
	}
	log.Println("Server gracefully stopped")
}
