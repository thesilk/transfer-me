VERSION=$(shell git describe --abbrev=4 --dirty --always --tags)
TAG_VERSION=$(shell head -n1 .version | cut -d "'" -f2)
TAG_SHORT_SHA=$(shell git rev-parse --short HEAD)
DEB_VERSION=${TAG_VERSION}-${TAG_SHORT_SHA}

UID=$(shell id -u)
GID=$(shell id -g)

GITLAB_REGISTRY=registry.gitlab.com
GITLAB_PROJECT=thesilk/transfer-me

LDFLAGS=-ldflags "-w -s -X main.version=${DEB_VERSION}"

APP_DEB_PACKAGE := transfer-me-${DEB_VERSION}.deb

${APP_DEB_PACKAGE}: debian/*
	@rm -rf out/
	@mkdir -p out/package/opt/transfer-me/public
	@mkdir -p out/package/etc/transfer-me
	@mkdir -p out/package/usr/local/bin
	@cp cmd/transfer-me out/package/opt/transfer-me/
	@cp ui/dist/* out/package/opt/transfer-me/public/.
	@cp debian/transfer-me.sh out/package/usr/local/bin/transfer-me
	@mkdir -p out/package/DEBIAN
	@cp debian/control out/package/DEBIAN/
	@cp debian/postinst out/package/DEBIAN/
	@sed -i s/@VERSION@/${DEB_VERSION}/g out/package/DEBIAN/control
	@chmod -R 0775 out/package/DEBIAN
	@find ./debian -type d | xargs chmod 777
	@dpkg-deb -Z xz -b out/package ${APP_DEB_PACKAGE}

DOCKER_PARAM=-it --rm\
			 -v ${PWD}:/app\
			 -v ${GOPATH}/pkg/mod/cache:/go/pkg/mod/cache\
			 --env UID=${UID}\
			 --env GID=${GID}\
			 -w /app\
			 ${GITLAB_REGISTRY}/${GITLAB_PROJECT}\

fmt:
	go fmt gitlab.com/...

vet:
	go vet gitlab.com/...

build-api:
	@go build ${LDFLAGS} -o cmd/transfer-me cmd/main.go

build-ui:
	@cd ui && npm run build

build: build-api build-ui

coverage: fmt vet
	mkdir -p coverage
	go test -coverpkg=./... -count=1 -v -coverprofile=coverage/coverage.out ./...
	go tool cover -func=coverage/coverage.out
	go tool cover -html=coverage/coverage.out -o coverage/coverage.html

package: clean build ${APP_DEB_PACKAGE}
	@echo ${APP_DEB_PACKAGE}

package-only: ${APP_DEB_PACKAGE}
	@echo ${APP_DEB_PACKAGE}

run: build
	@echo './transfer-me runs in version ${VERSION}'
	@./cmd/transfer-me

run-api: build-api
	@echo './transfer-me runs in version ${VERSION}'
	@./cmd/transfer-me

clean:
	@rm -f transfer-me*.db
	@rm -f transfer-me*.deb
	@rm -f transfer-me

install: package
	@sudo dpkg -i transfer-me*.deb

docker-image:
	@docker build -t ${GITLAB_REGISTRY}/${GITLAB_PROJECT} -f docker/build.Dockerfile .

docker-gitlab-registry-push: docker-image
	docker login ${GITLAB_REGISTRY}
	docker push ${GITLAB_REGISTRY}/${GITLAB_PROJECT}

docker-build:
	@docker run ${DOCKER_PARAM} /bin/bash -c "make build && chown ${UID}:${GID} transfer-me"

docker-test:
	@docker run ${DOCKER_PARAM} make test

docker-coverage:
	@docker run ${DOCKER_PARAM} make coverage

docker-package:
	@docker run ${DOCKER_PARAM} /bin/bash -c "make package && chown ${UID}:${GID} transfer-me transfer-me*.deb && rm -rf out/"

docker-transfer-me-build:

docker-transfer-me-run:
	@docker run -v ./app/config.yaml:/etc/ /bin/bash -c "make package && chown ${UID}:${GID} transfer-me transfer-me*.deb && rm -rf out/"

docker-minio-run:
	@docker run -d --rm -p 9000:9000 -p 9090:9090 --name minio -v MINIO_CONTAINER:/data -e "MINIO_ROOT_USER=ROOTNAME" -e "MINIO_ROOT_PASSWORD=CHANGEME123" quay.io/minio/minio server /data --console-address ":9090"

docker-minio-stop:
	@docker stop minio


.PHONY: fmt\
		vet\
		build\
		test\
		coverage\
		package\
		run\
		clean\
		install\
		docker-image\
		docker-gitlab-registry-push\
		docker-build\
		docker-test\
		docker-coverage\
		docker-package
