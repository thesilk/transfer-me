FROM debian:bookworm
LABEL maintainer=TheSilk

RUN apt update && apt upgrade -y

COPY transfer-me*.deb /tmp/transfer-me.deb
RUN dpkg -i /tmp/transfer-me.deb

ENV GIN_MODE=release

WORKDIR /opt/transfer-me
ENTRYPOINT ["/usr/local/bin/transfer-me"]
EXPOSE 2342/tcp
