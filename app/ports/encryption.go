package ports

import "io"

type EncryptionService interface {
	Encrypt(file io.Reader, key []byte) (io.Reader, error)
	Decrypt(cipherFile io.Reader, key []byte) (io.Reader, error)
	GetSecret() string
}
