package ports

import (
	"io"
	"mime/multipart"

	"github.com/minio/minio-go/v7"

	"gitlab.com/thesilk/transfer-me/app/models"
)

type S3FileRepo interface {
	GetS3File(fileName string) (*minio.Object, error)
	PutS3File(fileName string, imageReader io.Reader, fileSize int64) error
}

type S3FilService interface {
	Download(bundle, id, key string) ([]byte, error)
	DownloadStat(bundle string) (*models.UploadStat, error)
	Upload(fileHeaders []*multipart.FileHeader) (*models.UploadStat, error)
	UploadStat(uploadStat *models.UploadStat) error
}
