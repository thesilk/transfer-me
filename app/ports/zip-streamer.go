package ports

import (
	"io"

	"gitlab.com/thesilk/transfer-me/app/models"
)

type ZipStreamerService interface {
	Stream(writer io.Writer, stats *models.UploadStat, key []byte)
}
