package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/thesilk/transfer-me/app/ports"
)

func PostDownloadBundle(s3Service ports.S3FilService, zipStreamService ports.ZipStreamerService) gin.HandlerFunc {
	return func(c *gin.Context) {
		bundle := c.Param("bundle")
		key := c.Query("key")

		c.Header("Content-Type", "application/zip")
		c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s.zip", bundle))

		uploadedFiles, err := s3Service.DownloadStat(bundle)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		c.Status(http.StatusOK)
		zipStreamService.Stream(c.Writer, uploadedFiles, []byte(key))
	}
}

func GetDownloadStatsForBundle(s3Service ports.S3FilService, validToken string) gin.HandlerFunc {
	return func(c *gin.Context) {
		bundle := c.Param("bundle")

		uploadedFiles, err := s3Service.DownloadStat(bundle)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		c.JSON(http.StatusOK, uploadedFiles)
	}
}
