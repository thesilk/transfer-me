package controller

import (
	"log"
	"net/http"
	"runtime"

	"github.com/gin-gonic/gin"

	"gitlab.com/thesilk/transfer-me/app/ports"
)

func PutFileUpload(s3Service ports.S3FilService, validToken string) gin.HandlerFunc {
	return func(c *gin.Context) {
		var m runtime.MemStats
		runtime.ReadMemStats(&m)

		log.Printf("[%s]: start request to upload files (allocated RAM: %d MiB)\n", c.ClientIP(), m.Alloc/1024/1024)
		token := c.Request.Header.Get("Token")

		if token != validToken {
			log.Printf("[%s]: Unauthorized request with invalid token %s\n", c.ClientIP(), token)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		form, _ := c.MultipartForm()
		fileHeaders := form.File["files"]

		fileUploadStat, err := s3Service.Upload(fileHeaders)
		if err != nil {
			log.Printf("[%s]: %s\n", c.ClientIP(), err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		key := fileUploadStat.Key
		fileUploadStat.Key = ""

		if err := s3Service.UploadStat(fileUploadStat); err != nil {
			log.Printf("[%s]: %s\n", c.ClientIP(), err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		fileUploadStat.Key = key

		c.JSON(http.StatusOK, fileUploadStat)

		log.Printf("[%s]: uploaded files successfully (allocated RAM %d MiB)\n", c.ClientIP(), m.Alloc/1024/1024)
		return
	}
}
