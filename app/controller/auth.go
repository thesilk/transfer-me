package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func PostAuth(validToken string) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Token")

		if token != validToken {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		c.Status(http.StatusAccepted)
	}
}
