package controller

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/thesilk/transfer-me/app/ports"
)

func GetDownloadFile(s3Service ports.S3FilService) gin.HandlerFunc {
	return func(c *gin.Context) {
		bundle := c.Param("bundle")
		id := c.Param("id")
		key := c.Query("key")

		uploadedFiles, err := s3Service.DownloadStat(bundle)
		if err != nil {
			log.Printf("[%s]: %s", c.ClientIP(), err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		fileBuffer, err := s3Service.Download(bundle, id, key)
		if err != nil {
			log.Printf("[%s]: %s", c.ClientIP(), err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		fileStat, err := uploadedFiles.FilterById(id)
		if err != nil {
			log.Printf("[%s]: %s", c.ClientIP(), err)
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		contentDisposition := fmt.Sprintf("attachment; filename=%s", fileStat.Name)
		c.Header("Content-Disposition", contentDisposition)
		c.Data(200, fileStat.ContentType, fileBuffer)
		return
	}
}
