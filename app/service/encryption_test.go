package service

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncryptionServiceIntegration(t *testing.T) {
	type testCases struct {
		name     string
		filePath string
		result   string
	}

	secret := "abcdefghijklmnopqrstuvwxyz012345"
	es := NewEncryptionService(secret)
	key := []byte(secret)

	cases := []testCases{
		{
			name:     "short plain text",
			filePath: "plain-short.txt",
			result:   "01234567\n",
		},
		{
			name:     "plain text",
			filePath: "plain.txt",
			result:   "0123456789abcdef\n",
		},
		{
			name:     "long plain text",
			filePath: "plain-long.txt",
			result:   "Hello this is a long text.\n",
		},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			file, _ := os.Open(fmt.Sprintf("../../fixtures/encryption/%s", testCase.filePath))
			defer file.Close()

			reader := bufio.NewReader(file)

			encryptedReader, err := es.Encrypt(reader, key)

			assert.Nil(t, err)

			decryptedReader, err := es.Decrypt(encryptedReader, key)

			assert.Nil(t, err)

			contentBytes, err := io.ReadAll(decryptedReader)

			assert.Nil(t, err)
			assert.Equal(t, testCase.result, string(contentBytes))
		})

	}
}
