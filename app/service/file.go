package service

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"time"

	"github.com/minio/minio-go/v7"

	"gitlab.com/thesilk/transfer-me/app/models"
	"gitlab.com/thesilk/transfer-me/app/ports"
)

type S3FileService struct {
	repository        ports.S3FileRepo
	encryptionService ports.EncryptionService
	StatsFileName     string
}

func NewS3FileService(rep ports.S3FileRepo, encryptSvc ports.EncryptionService) ports.S3FilService {
	return &S3FileService{
		repository:        rep,
		StatsFileName:     "stats.json",
		encryptionService: encryptSvc,
	}
}

func (s *S3FileService) Download(bundle, id, key string) ([]byte, error) {
	var (
		err        error
		file       *minio.Object
		fileBuffer []byte
	)
	s3FilePath := fmt.Sprintf("%s/%s.binary", bundle, id)

	if file, err = s.repository.GetS3File(s3FilePath); err != nil {
		return fileBuffer, fmt.Errorf("DownloadService: Couldn't download %s: %v", s3FilePath, err)
	}
	defer file.Close()

	decryptedFile, err := s.encryptionService.Decrypt(file, []byte(key))
	if err != nil {
		return fileBuffer, fmt.Errorf("DownlaodService: couldn't decrypt file %s: %v", s3FilePath, err)
	}

	if fileBuffer, err = io.ReadAll(decryptedFile); err != nil {
		return fileBuffer, fmt.Errorf("DownloadService: Couldn't read %s: %v", s3FilePath, err)
	}

	return fileBuffer, nil
}

func (s *S3FileService) DownloadStat(bundle string) (*models.UploadStat, error) {
	var (
		err        error
		file       *minio.Object
		fileBuffer []byte
		uploadStat models.UploadStat
	)
	statsFilePath := fmt.Sprintf("%s/%s", bundle, s.StatsFileName)

	if file, err = s.repository.GetS3File(statsFilePath); err != nil {
		return &uploadStat, fmt.Errorf("DownloadStatService: Couldn't download %s: %v", statsFilePath, err)
	}
	defer file.Close()

	if fileBuffer, err = io.ReadAll(file); err != nil {
		return &uploadStat, fmt.Errorf("DownloadStatService: Couldn't read %s: %v", statsFilePath, err)
	}

	if err = json.Unmarshal(fileBuffer, &uploadStat); err != nil {
		return &uploadStat, fmt.Errorf("DownloadStatService: couldn't unmarshal stats.json: %v", err)
	}

	return &uploadStat, nil
}

func (s *S3FileService) createRandomKey() []byte {
	secret := []byte(s.encryptionService.GetSecret())

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	r.Shuffle(len(secret), func(i, j int) {
		secret[i], secret[j] = secret[j], secret[i]
	})

	return secret[:16]
}

func (s *S3FileService) Upload(fileHeaders []*multipart.FileHeader) (*models.UploadStat, error) {
	bundleHash := s.createHash()
	key := s.createRandomKey()

	uploadStat := models.UploadStat{
		BundleName: bundleHash,
		UploadTime: time.Now(),
		Key:        string(key),
		Files:      []models.UploadedFile{},
	}

	for _, header := range fileHeaders {
		uploadedFile := models.UploadedFile{
			Name:             header.Filename,
			ContentType:      header.Header.Get("Content-Type"),
			UploadSuccessful: true,
		}

		file, err := header.Open()
		if err != nil {
			return &uploadStat, fmt.Errorf("UploadService: couldn't open uploaded file: %v", err)
		}
		defer file.Close()

		data, err := io.ReadAll(file)
		if err != nil {
			return &uploadStat, fmt.Errorf("UploadService: couldn't read uploaded file handle: %v", err)
		}

		bytesHash := md5.Sum(data)
		hash := hex.EncodeToString(bytesHash[:])
		uploadedFile.Hash = hash
		fileName := fmt.Sprintf("%s/%s.binary", bundleHash, hash)

		file.Seek(0, io.SeekStart)

		encryptedFile, err := s.encryptionService.Encrypt(file, key)
		if err != nil {
			return &uploadStat, fmt.Errorf("UploadService: couldn't encrypt file %s: %v", uploadedFile.Name, err)
		}

		// encrypted files is bigger than the origin file because it includes a header with the length of the key
		if err := s.repository.PutS3File(fileName, encryptedFile, header.Size+int64(len(key))); err != nil {
			log.Printf("UploadService: couldn't upload file to S3: %v", err)
			uploadedFile.UploadSuccessful = false
		}

		uploadStat.Files = append(uploadStat.Files, uploadedFile)
	}
	return &uploadStat, nil
}

func (s *S3FileService) UploadStat(uploadStat *models.UploadStat) error {
	var (
		buf  []byte
		err  error
		file io.Reader
	)

	statsPath := fmt.Sprintf("%s/%s", uploadStat.BundleName, s.StatsFileName)

	if buf, err = json.MarshalIndent(uploadStat, "", " "); err != nil {
		return fmt.Errorf("UploadStatService: couldn't marshal upload stats: %v", err)
	}

	file = bytes.NewReader(buf)

	if err := s.repository.PutS3File(statsPath, file, int64(len(buf))); err != nil {
		return fmt.Errorf("UploadStatsService: couldn't upload stats.json to S3: %v", err)
	}

	return nil
}

func (s *S3FileService) createHash() string {
	currentUnixTime := fmt.Sprintf("%d", time.Now().Unix())

	return fmt.Sprintf("%x", md5.Sum([]byte(currentUnixTime)))
}
