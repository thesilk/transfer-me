package service

import (
	"archive/zip"
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/minio/minio-go/v7"

	"gitlab.com/thesilk/transfer-me/app/models"
	"gitlab.com/thesilk/transfer-me/app/ports"
)

type zipStreamerService struct {
	encryptionService ports.EncryptionService
	repository        ports.S3FileRepo
}

func NewZipStreamerService(rep ports.S3FileRepo, encryptSvc ports.EncryptionService) ports.ZipStreamerService {
	return &zipStreamerService{repository: rep, encryptionService: encryptSvc}
}

func (z *zipStreamerService) Stream(writer io.Writer, stats *models.UploadStat, key []byte) {
	var (
		file *minio.Object
	)

	zipWriter := zip.NewWriter(writer)

	for _, entry := range stats.Files {
		header := &zip.FileHeader{
			Name:     entry.Name,
			Method:   zip.Store, // deflate also works, but at a cost
			Modified: time.Now(),
		}
		entryWriter, err := zipWriter.CreateHeader(header)
		if err != nil {
			log.Println(err)
		}

		s3FilePath := fmt.Sprintf("%s/%s.binary", stats.BundleName, entry.Hash)

		if file, err = z.repository.GetS3File(s3FilePath); err != nil {
			log.Printf("DownloadService: Couldn't download %s: %v\n", s3FilePath, err)
			continue
		}
		defer file.Close()

		decryptedFile, err := z.encryptionService.Decrypt(file, key)
		if err != nil {
			log.Printf("DownlaodService: couldn't decrypt file %s: %v", s3FilePath, err)
			continue
		}

		fileReader := bufio.NewReader(decryptedFile)

		_, err = io.Copy(entryWriter, fileReader)
		if err != nil {
			log.Println(err)
			continue
		}

		zipWriter.Flush()
		flushingWriter, ok := writer.(http.Flusher)
		if ok {
			flushingWriter.Flush()
		}
	}

	zipWriter.Close()

}
