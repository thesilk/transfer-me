package service

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"

	"gitlab.com/thesilk/transfer-me/app/ports"
)

type encryptionService struct {
	secret string
}

func NewEncryptionService(secret string) ports.EncryptionService {
	return &encryptionService{secret: secret}
}

func (s *encryptionService) GetSecret() string {
	return s.secret
}

func (s *encryptionService) Encrypt(file io.Reader, key []byte) (io.Reader, error) {
	fileBuffer, err := io.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("Encryption: Couldn't read file: %v", err)
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("Encryption: Couldn't create cipher block: %v", err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(fileBuffer))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, fmt.Errorf("Encryption: Couldn't create random IV: %v", err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], fileBuffer)

	return bytes.NewReader(ciphertext), nil
}

func (s *encryptionService) Decrypt(cipherFile io.Reader, key []byte) (io.Reader, error) {
	var (
		cipherData []byte
		err        error
	)

	if cipherData, err = io.ReadAll(cipherFile); err != nil {
		return nil, fmt.Errorf("Decryption: Couldn't read encrypted file: %v", err)
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("Decryption: Couldn't create cipher block: %v", err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(cipherData) < aes.BlockSize {
		return nil, fmt.Errorf("Decryption: encrypted data has to be bigger than 16 bytes")
	}
	iv := cipherData[:aes.BlockSize]
	cipherData = cipherData[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(cipherData, cipherData)

	return bytes.NewReader(cipherData), nil
}
