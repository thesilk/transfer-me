package repository

import (
	"context"
	"fmt"
	"io"

	"github.com/minio/minio-go/v7"
)

func (r *Repository) PutS3File(fileName string, imageReader io.Reader, fileSize int64) error {

	_, err := r.S3Client.PutObject(context.Background(), r.s3Config.BucketName, fileName, imageReader, fileSize, minio.PutObjectOptions{ContentType: "application/octet-stream"})
	if err != nil {
		return fmt.Errorf("Couldn't upload %s to S3 storage: %v", fileName, err)
	}

	return nil
}

func (r *Repository) GetS3File(fileName string) (*minio.Object, error) {
	return r.S3Client.GetObject(context.Background(), r.s3Config.BucketName, fileName, minio.GetObjectOptions{})
}
