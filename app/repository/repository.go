package repository

import (
	"context"
	"log"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"gitlab.com/thesilk/transfer-me/app/models"
)

type Repository struct {
	S3Client *minio.Client
	s3Config models.S3Storage
}

func NewRepository(s3Config models.S3Storage) (*Repository, error) {
	var (
		r   *Repository = &Repository{}
		err error
	)

	r.s3Config = s3Config

	if err = r.initS3Client(s3Config); err != nil {
		return r, err
	}

	return r, nil
}

func (r *Repository) initS3Client(s3Config models.S3Storage) error {
	var err error

	ctx := context.Background()

	r.S3Client, err = minio.New(s3Config.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(s3Config.AccessKey, s3Config.SecretKey, ""),
		Secure: s3Config.UseSSL,
	})
	if err != nil {
		return err
	}

	err = r.S3Client.MakeBucket(ctx, s3Config.BucketName, minio.MakeBucketOptions{Region: s3Config.Location})
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, errBucketExists := r.S3Client.BucketExists(ctx, s3Config.BucketName)
		if errBucketExists == nil && exists {
			log.Printf("Skipping creation of the bucked \"%s\"", s3Config.BucketName)
		} else {
			return err
		}
	} else {
		log.Printf("Successfully created %s\n", s3Config.BucketName)
	}
	return nil

}
