package models

import (
	"fmt"
	"time"
)

type UploadStat struct {
	BundleName string         `json:"bundleName"`
	UploadTime time.Time      `json:"uploadTime"`
	Key        string         `json:"key"`
	Files      []UploadedFile `json:"files"`
}

type UploadedFile struct {
	Name             string `json:"name"`
	Hash             string `json:"hash"`
	ContentType      string `json:"contentType"`
	UploadSuccessful bool   `json:"uploadSuccessful"`
}

func (u *UploadStat) FilterById(id string) (*UploadedFile, error) {
	for _, fileStat := range u.Files {
		if fileStat.Hash == id {
			return &fileStat, nil
		}
	}
	return &UploadedFile{}, fmt.Errorf("StatFiles: couldn't find given file by id %s", id)

}
