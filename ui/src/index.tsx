import React from "react";
import { createRoot, Root } from "react-dom/client";
import { HashRouter } from "react-router-dom";

import { App } from "./App";

import "./index.scss";

const container = document.getElementById("root");

if (!container) {
  throw new Error("no #root entry point found");
}

const root: Root = createRoot(container);

root.render(
  <HashRouter>
    <App />
  </HashRouter>
);
