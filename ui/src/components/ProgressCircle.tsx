import React from "react";
import "./ProgressCircle.scss";

interface IProps {
  radius: number;
  stroke: number;
  progress: number;
  fontSize: number;
}

const ProgressCircle = (props: IProps) => {
  const normalizedRadius = props.radius - props.stroke * 2;
  const circumference = normalizedRadius * 2 * Math.PI;
  const strokeDashoffset = circumference - (props.progress / 100) * circumference;

  return (
    <svg height={props.radius * 2} width={props.radius * 2}>
      <text
        x={props.radius - (3 * props.fontSize) / 2}
        y={props.radius + props.fontSize / 3}
        style={{ fontSize: `${props.fontSize}px` }}
      >
        {props.progress}%
      </text>
      <circle
        stroke='white'
        fill='transparent'
        strokeWidth={props.stroke}
        strokeDasharray={circumference + " " + circumference}
        style={{ strokeDashoffset }}
        r={normalizedRadius}
        cx={props.radius}
        cy={props.radius}
      />
    </svg>
  );
};

export default ProgressCircle;
