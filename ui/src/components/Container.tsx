import React from "react";
import { useNavigate, useLocation, NavigateFunction } from "react-router-dom";
import { FaArrowLeft } from "@react-icons/all-files/fa/FaArrowLeft";

import "./Container.scss";

const Container = ({ children }) => {
  const navigate: NavigateFunction = useNavigate();

  return (
    <div className='outer-container'>
      <button onClick={() => navigate("/")} style={{ display: location.hash.includes("stats") ? "flex" : "none" }}>
        <FaArrowLeft />
      </button>
      <div className='inner-container'>{children}</div>
    </div>
  );
};

export default Container;
