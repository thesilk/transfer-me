import React from "react";

import "./UploadProgressbar.scss";

interface IProps {
  progress: number;
  total: number;
  loaded: number;
  estimate: number;
}

const UploadProgressbar = (props: IProps) => {
  const transformInMB = (value: number) => {
    return (value / (1024 * 1024)).toFixed(1) + "MB";
  };

  const convertTime = (value: number) => {
    let rest = 0;
    if (value > 3600) {
      const hours = value / 3600;
      rest = (hours - Math.floor(value / 3600)) * 3600;
      return `${Math.floor(hours)}h ${convertTime(rest)}`;
    } else if (value > 60) {
      const minutes = value / 60;
      rest = (minutes - Math.floor(value / 60)) * 60;
      return `${Math.floor(minutes)}min ${convertTime(rest)}`;
    } else {
      return `${value.toFixed(0)}s`;
    }
  };

  return (
    <div className='upload-progressbar' style={{ display: props.progress === 0 ? "none" : "block" }}>
      <progress value={props.progress} max={100}></progress>
      <div className='upload-progressbar-help-text'>
        {transformInMB(props.loaded)}/{transformInMB(props.total)} ({convertTime(props.estimate)})
      </div>
    </div>
  );
};

export default UploadProgressbar;
