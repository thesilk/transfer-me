import axios, { AxiosResponse } from "axios";
import { saveAs } from "file-saver";
import React, { useEffect, useState } from "react";
import { BsCardImage } from "@react-icons/all-files/bs/BsCardImage";
import { BsGearWideConnected } from "@react-icons/all-files/bs/BsGearWideConnected";
import { BsMusicNoteBeamed } from "@react-icons/all-files/bs/BsMusicNoteBeamed";
import { BsCameraVideo } from "@react-icons/all-files/bs/BsCameraVideo";
import { FaRegFilePdf } from "@react-icons/all-files/fa/FaRegFilePdf";
import { BsFileEarmarkZip } from "@react-icons/all-files/bs/BsFileEarmarkZip";
import { BsDownload } from "@react-icons/all-files/bs/BsDownload";
import { BsFileEarmark } from "@react-icons/all-files/bs/BsFileEarmark";
import { FaShareAlt } from "@react-icons/all-files/fa/FaShareAlt";
import { useLocation } from "react-router-dom";

import Container from "../components/Container";
import { IStatFiles, IStats } from "../interfaces/stats.interface";
import "./Stats.scss";

const Stats = () => {
  const location = useLocation();
  const token: string | null = localStorage.getItem("token") || new URLSearchParams(location.search).get("token") || "";
  const [stats, setStats] = useState({ bundleName: "", key: "-", files: [] } as IStats);
  const [isLoadingBundle, setIsLoadingBundle] = useState(false);

  useEffect(() => {
    const stats: IStats | undefined = location.state;

    if (!stats) {
      const key = new URLSearchParams(location.search).get("key") || "";
      const bundleName = location.pathname?.split("/")[2] ?? "";
      axios.get<IStats>("/api/v0/bundle/" + bundleName).then((res: AxiosResponse<IStats>) => {
        setStats({ ...res.data, key });
      });
    } else {
      setStats(stats);
    }
  }, []);

  const contentType = {
    image: <BsCardImage className='download-content-type' />,
    "application/pdf": <FaRegFilePdf className='download-content-type' />,
    video: <BsCameraVideo className='download-content-type' />,
    audio: <BsMusicNoteBeamed className='download-content-type' />,
  };
  const getContentTypeIcon = (contentTypeStat: string) => {
    if (contentTypeStat === "application/pdf") {
      return <FaRegFilePdf className='download-content-type' />;
    }
    return contentType[contentTypeStat.split("/")[0]] || <BsFileEarmark className='download-content-type' />;
  };

  const createBundleItem = () => {
    return (
      <li className='download-item'>
        <BsFileEarmarkZip className='download-content-type' />
        <span>bundle.zip</span>
        <button onClick={() => downloadBundleFile()}>
          {isLoadingBundle ? <BsGearWideConnected className='bundle-gear-spinner' /> : <BsDownload />}
        </button>
      </li>
    );
  };

  const downloadFile = (file: IStatFiles) => {
    axios
      .post(`/api/v0/download/${stats.bundleName}/${file.hash}?key=${stats.key}`, null, {
        responseType: "blob",
        headers: { Token: token },
      })
      .then((response: AxiosResponse<Blob>) => {
        const downloadFile = new Blob([response.data], { type: file.contentType });
        saveAs(downloadFile, file.name);
      });
  };

  const downloadBundleFile = () => {
    setIsLoadingBundle(true);
    axios
      .post(`/api/v0/bundle/${stats.bundleName}?key=${stats.key}`, null, {
        responseType: "blob",
        headers: { Token: token },
      })
      .then((response: AxiosResponse<Blob>) => {
        const downloadBundle = new Blob([response.data], { type: "application/zip" });
        saveAs(downloadBundle, "bundle.zip");
        setIsLoadingBundle(false);
      });
  };

  const createFileItems = () => {
    return stats.files.map((file: IStatFiles, index: number) => {
      return (
        <li className='download-item' key={index}>
          {getContentTypeIcon(file.contentType)}
          <span title={file.name}>{file.name}</span>
          <button onClick={() => downloadFile(file)}>
            <BsDownload />
          </button>
        </li>
      );
    });
  };

  const createShareableLink = () => {
    if (location.pathname === "/stats") {
      return `${window.location.href}/${stats.bundleName}?key=${stats.key}`;
    }
    return window.location.href;
  };

  const shareHandler = () => {
    if (navigator.share) {
      navigator.share({ url: createShareableLink(), title: "TransferMe" }).then(console.log);
    } else if (navigator.clipboard) {
      navigator.clipboard.writeText(createShareableLink()).then(console.log);
    } else {
      console.error("no sharing option provided");
    }
  };

  return (
    <Container>
      <div className='stats-container'>
        <h3>Link teilen</h3>
        <div className='link-share'>
          <input readOnly value={createShareableLink()} />
          <button onClick={() => shareHandler()}>
            <FaShareAlt />
          </button>
        </div>
        <h3>Bundle</h3>
        <ul className='bundle'>{createBundleItem()}</ul>
        <h3>Dateien</h3>
        <ul className='files'>{createFileItems()}</ul>
      </div>
    </Container>
  );
};

export default Stats;
