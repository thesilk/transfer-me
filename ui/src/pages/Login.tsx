import axios from "axios";
import React, { ChangeEvent, MouseEvent, useEffect, useState } from "react";
import { BsFillUnlockFill } from "@react-icons/all-files/bs/BsFillUnlockFill";

import "./Login.scss";
import Container from "../components/Container";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [token, setToken] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    checkAuthorization().then((authorized: boolean) => {
      authorized && navigate("/");
    });
  });

  const changeHandler = (ev: ChangeEvent<HTMLInputElement>) => {
    setToken(ev.target.value);
  };

  const clickHandler = (_ev: MouseEvent<HTMLButtonElement>) => {
    localStorage.setItem("token", token);
    navigate("/");
  };

  const checkAuthorization = async () => {
    let authorized = false;
    let token: string | null = localStorage.getItem("token") || new URLSearchParams(location.search).get("token");

    if (token) {
      await axios.post("/api/v0/auth", {}, { headers: { Token: token } });
      authorized = true;
      localStorage.setItem("token", token ?? "");
    }

    return authorized;
  };

  return (
    <Container>
      <div className='login-container'>
        <h1>TransferMe freischalten</h1>
        <p>
          Um Zugang zu diesem Service zu bekommen, wird ein Token benötigt. Nur Freunde und Familie haben diesen Token,
          da diese Anwendung rein privat ist.
        </p>
        <form>
          <input id='token-input' placeholder='token' onChange={(ev) => changeHandler(ev)} />
          <button onClick={(ev) => clickHandler(ev)}>
            <BsFillUnlockFill />
          </button>
        </form>
      </div>
    </Container>
  );
};

export default Login;
