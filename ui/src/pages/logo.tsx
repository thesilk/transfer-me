import React, { useEffect, useState } from "react";
import ProgressCircle from "../components/ProgressCircle";

const Logo = () => {
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    const timer = setInterval(
      () =>
        setProgress((previousProgress: number) => {
          if (previousProgress + 5.1 > 101) return 0;
          return +(previousProgress + 5.1).toFixed(1);
        }),
      100
    );
    return () => clearInterval(timer);
  });

  return (
    <div>
      <ProgressCircle radius={100} progress={progress} stroke={10} fontSize={24} />
    </div>
  );
};

export default Logo;
