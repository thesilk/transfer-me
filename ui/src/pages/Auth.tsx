import React from "react";
import { BsFillLockFill } from "@react-icons/all-files/bs/BsFillLockFill";

import "./Auth.scss";
import Container from "../components/Container";

const Unauthorized = () => {
  return (
    <Container>
      <div className='auth-container'>
        <h1>Kein Zugang</h1>
        <BsFillLockFill className='lock-icon' />
        <p>
          Diese Seite ist privat und nur zugänglich für Familie und Freunde. Es werden keine kommerziellen Absichten mit
          dieser Seite erzielt.
        </p>
      </div>
    </Container>
  );
};

export default Unauthorized;
