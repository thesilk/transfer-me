import "./Upload.scss";
import React, { ChangeEvent, useRef, useState } from "react";
import { BsPlus } from "@react-icons/all-files/bs/BsPlus";
import Container from "../components/Container";
import axios, { AxiosProgressEvent } from "axios";
import { useLocation, useNavigate } from "react-router-dom";
import ProgressCircle from "../components/ProgressCircle";

interface IProgress {
  progress: number;
  total: string;
  loaded: string;
  estimate: string;
}

export const Upload = () => {
  const location = useLocation();
  const token: string | null = localStorage.getItem("token") || new URLSearchParams(location.search).get("token") || "";
  const navigate = useNavigate();
  const inputRef = useRef<HTMLInputElement>(null);
  const [progress, setProgress] = useState<IProgress | null>(null);

  const fileUploadHandler = (_ev: ChangeEvent<HTMLInputElement>): void => {
    const filesToUpload = inputRef.current?.files;
    const formData = new FormData();

    if (filesToUpload) {
      Array.from(filesToUpload).forEach((file: File) => {
        formData.append("files", file, file.name);
      });
    }

    axios
      .put("/api/v0/upload", formData, {
        onUploadProgress: (ev: AxiosProgressEvent) => {
          if (ev) {
            setProgress({
              progress: 100 * (ev?.progress ?? 0),
              total: transformInMB(ev?.total ?? 0),
              loaded: transformInMB(ev?.loaded ?? 0),
              estimate: convertTime(ev?.estimated ?? 0),
            });
          }
        },
        headers: { Token: token, "Content-Type": "multipart/form-data" },
      })
      .then((res) => navigate("/stats", { state: res.data }));
  };

  const transformInMB = (value: number) => {
    return (value / (1024 * 1024)).toFixed(1) + "MB";
  };

  const convertTime = (value: number) => {
    let rest = 0;
    if (value > 3600) {
      const hours = value / 3600;
      rest = (hours - Math.floor(value / 3600)) * 3600;
      return `${Math.floor(hours)}h ${convertTime(rest)}`;
    } else if (value > 60) {
      const minutes = value / 60;
      rest = (minutes - Math.floor(value / 60)) * 60;
      return `${Math.floor(minutes)}min ${convertTime(rest)}`;
    } else {
      return `${value.toFixed(0)}s`;
    }
  };

  return (
    <Container>
      <div className='upload-container'>
        {progress && (
          <div className='upload-progress'>
            <ProgressCircle radius={130} progress={+progress.progress.toFixed(1)} stroke={10} fontSize={24} />
            <h1>Dateien werden hochgeladen...</h1>
            <p>Deine Dateien werden im Moment hochgeladen. Bitte habe einen Moment Geduld.</p>
            <div className='upload-progressbar-help-text'>
              {progress.loaded} / {progress.total} ({progress.estimate}))
            </div>
          </div>
        )}
        {progress === null && (
          <div className='upload-area'>
            <img src='transfer_me.svg' />
            <h1>Dateien hochladen</h1>
            <p className='max-file-size-info'>Maximale Uploadgröße: 2 GB</p>
            <p>
              Wähle alle Dateien, die du hochladen möchtest, aus. Nach dem erfolgreichen Hochladen wird dir ein Link zur
              Verfügung gestellt, die du jeder Person schicken kannst, die diese Dateien erhalten sollen. Alle Dateien
              sind Ende-zu-Ende verschlüsselt, so dass nur du und der Link Empfänger die Dateien lesen können.
            </p>
            <div>
              <label>
                <BsPlus />
                <input ref={inputRef} type='file' onChange={fileUploadHandler} multiple />
              </label>
            </div>
          </div>
        )}
      </div>
    </Container>
  );
};
