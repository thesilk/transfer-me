import axios from "axios";
import { useEffect } from "react";
import { useNavigate, useLocation, NavigateFunction } from "react-router-dom";

const AuthGuard = ({ children }) => {
  const location = useLocation();
  const navigate: NavigateFunction = useNavigate();

  const checkAuthorization = async () => {
    let authorized = false;
    let token: string | null = localStorage.getItem("token") || new URLSearchParams(location.search).get("token");

    if (token) {
      await axios.post("/api/v0/auth", {}, { headers: { Token: token } });
      authorized = true;
      localStorage.setItem("token", token ?? "");
    }

    return authorized;
  };

  useEffect(() => {
    if (!["unauthorized", "stats", "login"].includes(location.pathname.split("/")[1])) {
      checkAuthorization().then((authorized: boolean) => {
        if (!authorized) {
          navigate("/unauthorized");
        }
      });
    }
  }, [location.pathname]);

  return children;
};

export default AuthGuard;
