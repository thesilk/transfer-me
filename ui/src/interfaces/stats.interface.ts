export interface IStats {
  bundleName: string;
  key: string;
  files: IStatFiles[];
}

export interface IStatFiles {
  name: string;
  hash: string;
  contentType: string;
  uploadSuccessful: boolean;
}
