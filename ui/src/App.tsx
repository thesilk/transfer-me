import React, { createContext } from "react";
import { Route, Routes, useParams } from "react-router-dom";

import Unauthorized from "./pages/Auth";
import { Upload } from "./pages/Upload";
import RootGuard from "./guards/RootGuard";
import Stats from "./pages/Stats";
import Login from "./pages/Login";

export const AuthorizationGuard = createContext(false);

export function App() {
  return (
    <RootGuard>
      <Routes>
        <Route path='/unauthorized' element={<Unauthorized />}></Route>
        <Route path='/' element={<Upload />}></Route>
        <Route path='/login' element={<Login />}></Route>
        <Route path='/stats' element={<Stats />}></Route>
        <Route path='/stats/:id' element={<Stats />}></Route>
        <Route path='*' element={<NotFound />} />
      </Routes>
    </RootGuard>
  );
}

const NotFound = () => {
  const params = useParams();
  return <div>Could not found {params["*"]}</div>;
};
