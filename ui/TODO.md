- write deb definition
- write docker-compose
- configure tls


- write modal container component which is used by all views
- write upload file status component
    - icon depends on contentType
    - load cicle
    - filename as tooltip
    - file size as caption
- write files list component
  - uploaded files goes to the bottom
  - current download at the top
- write file upload component
- write upload page
  - navigates to stat file -> prop in case of forwarding otherwise fetch
- write stat page
  - icon (contentType) + filename + download state + download button

