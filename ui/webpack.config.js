const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const isDevelopment = process.env.DEVELOPMENT === "true";

module.exports = {
  mode: isDevelopment ? "development" : "production",
  entry: "./src/index.tsx",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"],
  },
  output: {
    path: path.resolve(__dirname, "dist/"),
    publicPath: "/",
    filename: "[name].[contenthash].js",
  },
  devtool: isDevelopment ? "source-map" : false,
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        use: ["babel-loader"],
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(css)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "./",
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./public/index.html",
      filename: "index.html",
    }),
    new CopyPlugin({
      patterns: [
        { from: "./public/style.css", to: "." },
        { from: "./public/manifest.json", to: "." },
        { from: "./public/transfer_me.svg", to: "." },
        { from: "./public/logo192.png", to: "." },
        { from: "./public/favicon.ico", to: "." },
      ],
    }),
  ],
  devServer: {
    proxy: {
      "/api/v0": {
        target: "http://localhost:2342",
      },
    },
  },
};
